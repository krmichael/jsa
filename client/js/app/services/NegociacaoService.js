'use strict';

System.register(['./HttpService', './ConnectionFactory', '../models/Negociacao', '../dao/NegociacaoDao'], function (_export, _context) {
  "use strict";

  var HttpService, ConnectionFactory, Negociacao, NegociacaoDao, _createClass, NegociacaoService;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  return {
    setters: [function (_HttpService) {
      HttpService = _HttpService.HttpService;
    }, function (_ConnectionFactory) {
      ConnectionFactory = _ConnectionFactory.ConnectionFactory;
    }, function (_modelsNegociacao) {
      Negociacao = _modelsNegociacao.Negociacao;
    }, function (_daoNegociacaoDao) {
      NegociacaoDao = _daoNegociacaoDao.NegociacaoDao;
    }],
    execute: function () {
      _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      _export('NegociacaoService', NegociacaoService = function () {
        function NegociacaoService() {
          _classCallCheck(this, NegociacaoService);

          this._http = new HttpService();
        }

        _createClass(NegociacaoService, [{
          key: 'obterNegociacoesDaSemana',
          value: function obterNegociacoesDaSemana() {
            var _this = this;

            return new Promise(function (resolve, reject) {

              _this._http.get('negociacoes/semana').then(function (negociacoes) {
                resolve(_this._map(negociacoes));
              }).catch(function (err) {
                console.log(err);
                reject('Não foi possível obter as negociações da semana.');
              });
            });
          }
        }, {
          key: 'obterNegociacoesDaSemanaAnterior',
          value: function obterNegociacoesDaSemanaAnterior() {
            var _this2 = this;

            return new Promise(function (resolve, reject) {

              _this2._http.get('negociacoes/anterior').then(function (negociacoes) {
                resolve(_this2._map(negociacoes));
              }).catch(function (err) {
                console.log(err);
                reject('Não foi possível obter as negociações da semana anterior.');
              });
            });
          }
        }, {
          key: 'obterNegociacoesDaSemanaRetrasada',
          value: function obterNegociacoesDaSemanaRetrasada() {
            var _this3 = this;

            return new Promise(function (resolve, reject) {

              _this3._http.get('negociacoes/retrasada').then(function (negociacoes) {
                resolve(_this3._map(negociacoes));
              }).catch(function (err) {
                console.log(err);
                reject('Não foi possível obter as negociações da semana retrasada');
              });
            });
          }
        }, {
          key: 'obterNegociacoes',
          value: function obterNegociacoes() {
            var _this4 = this;

            return new Promise(function (resolve, reject) {

              resolve(Promise.all([_this4.obterNegociacoesDaSemana(), _this4.obterNegociacoesDaSemanaAnterior(), _this4.obterNegociacoesDaSemanaRetrasada()]).then(function (negociacoes) {
                return negociacoes.reduce(function (arrayAchatado, array) {
                  return arrayAchatado.concat(array);
                }, []);
              }));

              reject(function (erro) {
                return console.log(erro);
              });
            });
          }
        }, {
          key: '_map',
          value: function _map(param) {

            return param.map(function (obj) {
              return new Negociacao(new Date(obj.data), obj.quantidade, obj.valor);
            });
          }
        }, {
          key: 'cadastra',
          value: function cadastra(negociacao) {

            return ConnectionFactory.getConnection().then(function (connection) {
              return new NegociacaoDao(connection);
            }).then(function (dao) {
              return dao.adiciona(negociacao);
            }).then(function () {
              return 'Negociação adicionada com sucesso.';
            }).catch(function () {
              throw new Error('Não foi possível adicionar a negociação');
            });
          }
        }, {
          key: 'lista',
          value: function lista() {

            return ConnectionFactory.getConnection().then(function (connection) {
              return new NegociacaoDao(connection);
            }).then(function (dao) {
              return dao.listaTodos();
            }).catch(function (erro) {
              console.log(erro);
              throw new Error('Não foi possível obter a lista de negociações.');
            });
          }
        }, {
          key: 'apaga',
          value: function apaga() {

            return ConnectionFactory.getConnection().then(function (connection) {
              return new NegociacaoDao(connection);
            }).then(function (dao) {
              return dao.apagarTodos();
            }).then(function (mensagem) {
              return mensagem;
            }).catch(function (erro) {
              console.log(erro);
              throw new Error(erro);
            });
          }
        }, {
          key: 'importar',
          value: function importar(listaAtual) {

            return this.obterNegociacoes().then(function (negociacoes) {
              return negociacoes.filter(function (negociacao) {
                return !listaAtual.some(function (negociacaoExistente) {
                  return JSON.stringify(negociacao) == JSON.stringify(negociacaoExistente);
                });
              });
            }).catch(function (erro) {
              console.log(erro);
              throw new Error('Não foi possível importar as negociações.');
            });
          }
        }]);

        return NegociacaoService;
      }());

      _export('NegociacaoService', NegociacaoService);
    }
  };
});
//# sourceMappingURL=NegociacaoService.js.map
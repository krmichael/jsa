export class DateHelper {

  constructor() {
    throw new Error('Esta classe não pode ser instânciada');
  }

  static textoParaData(texto) {
    //10/12/2018 === 2018-12-10
    if(!/\d{4}-\d{2}-\d{2}/.test(texto))
      throw new Error('A data deve estar no formato aaaa-mm-dd');

    return new Date(
      ...texto
      .split('-')
      .map((item, index) => item - index % 2)
    );
  }

  static dataParaTexto(data) {
    return `${DateHelper._format(data.getDate())}/${DateHelper._format(data.getMonth()+1)}/${data.getFullYear()}`;
  }

  static _format(value) {
    return value < 10 ? '0'+value : value;
  }
}
export class ProxyFactory {

  constructor() {
    throw new Error('Esta classe não pode ser instânciada');
  }

  static create(objeto, props, acao) {

    return new Proxy(objeto, {

      get(target, prop, receiver) {

        if (props.includes(prop) && ProxyFactory._isFunction(target[prop])) {

          return function () {

            console.log(`função ${prop} interceptada`);
            Reflect.apply(target[prop], target, arguments);
            return acao(target);
          }

        }
        return Reflect.get(target, prop, receiver);
      },

      set(target, prop, value, receiver) {

        if (props.includes(prop)) {

          console.log(`propriedade ${prop} interceptada`);
          target[prop] = value;
          acao(target);
        }
        return Reflect.set(target, prop, value, receiver);
      }
    });
  }

  static _isFunction(fnc) {
    return typeof (fnc) == typeof (Function);
  }
}
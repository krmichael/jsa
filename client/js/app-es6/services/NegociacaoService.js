import {HttpService} from './HttpService';
import {ConnectionFactory} from './ConnectionFactory';
import {Negociacao} from '../models/Negociacao';
import {NegociacaoDao} from '../dao/NegociacaoDao';

export class NegociacaoService {

  constructor() {

    this._http = new HttpService();
  }

  obterNegociacoesDaSemana() {

    return new Promise((resolve, reject) => {

      this._http.get('negociacoes/semana')
        .then(negociacoes => {
          resolve(
            this._map(negociacoes)
          )
        })
        .catch(err => {
          console.log(err);
          reject('Não foi possível obter as negociações da semana.');
        })
    })
  }

  obterNegociacoesDaSemanaAnterior() {

    return new Promise((resolve, reject) => {

      this._http.get('negociacoes/anterior')
        .then(negociacoes => {
          resolve(
            this._map(negociacoes)
          )
        })
        .catch(err => {
          console.log(err);
          reject('Não foi possível obter as negociações da semana anterior.');
        })
    })
  }

  obterNegociacoesDaSemanaRetrasada() {

    return new Promise((resolve, reject) => {

      this._http.get('negociacoes/retrasada')
        .then(negociacoes => {
          resolve(
            this._map(negociacoes)
          )
        })
        .catch(err => {
          console.log(err);
          reject('Não foi possível obter as negociações da semana retrasada');
        })
    })
  }

  obterNegociacoes() {

    return new Promise((resolve, reject) => {

      resolve(Promise.all([
        this.obterNegociacoesDaSemana(),
        this.obterNegociacoesDaSemanaAnterior(),
        this.obterNegociacoesDaSemanaRetrasada()
      ])
      .then(negociacoes => {
        return negociacoes.reduce((arrayAchatado, array) => arrayAchatado.concat(array), [])})
    );

      reject(erro => console.log(erro));
    })
  }

  _map(param) {

    return param.map(
      obj => new Negociacao(new Date(obj.data), obj.quantidade, obj.valor));
  }

  cadastra(negociacao) {

    return ConnectionFactory
      .getConnection()
      .then(connection => new NegociacaoDao(connection))
      .then(dao => dao.adiciona(negociacao))
      .then(() => 'Negociação adicionada com sucesso.')
      .catch(() => {
        throw new Error('Não foi possível adicionar a negociação');
      });
  }

  lista() {

    return ConnectionFactory
      .getConnection()
      .then(connection => new NegociacaoDao(connection))
      .then(dao => dao.listaTodos())
      .catch(erro => {
        console.log(erro);
        throw new Error('Não foi possível obter a lista de negociações.');
      });
  }

  apaga() {

    return ConnectionFactory
      .getConnection()
      .then(connection => new NegociacaoDao(connection))
      .then(dao => dao.apagarTodos())
      .then(mensagem => mensagem)
      .catch(erro => {
        console.log(erro);
        throw new Error(erro);
      });
  }

  importar(listaAtual) {

    return this.obterNegociacoes()
      .then(negociacoes => 
        negociacoes.filter(negociacao => 
          !listaAtual.some(negociacaoExistente =>
            JSON.stringify(negociacao) == JSON.stringify(negociacaoExistente)))
      )
      .catch(erro => {
        console.log(erro);
        throw new Error('Não foi possível importar as negociações.');
      });
  }

}